<?php

namespace App\Http\Controllers;

use App\Product;
use App\Page;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('featured', true)->take(8)->inRandomOrder()->get();
        if(request('page')){
            $page = Page::where('slug', request('page'))->firstOrFail();
        }else{
            $page = Page::firstOrFail();
        }
        
        return view('landing-page')->with('page', $page);
    }
}
