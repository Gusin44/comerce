@extends('layout', ['background' => Voyager::image( $page->image )])

@section('title', $page->title)

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
@endsection

@section('content')

    @component('components.breadcrumbs')
        <a href="/">Home</a>
        <i class="fa fa-chevron-right breadcrumb-separator"></i>
        <span>{{ $page->title }}</span>
    @endcomponent

    <div class="container">
        <div class="product-section-information">
            <div class="spacer"></div>
            <h1 class="product-section-title">{{ $page->title }}</h1>
            <div class="spacer"></div>
            <p><img src="@if( !filter_var($page->image, FILTER_VALIDATE_URL)){{ Voyager::image( $page->image ) }}@else{{ $page->image }}@endif"></p>
            <p>
                {!! $page->body !!}
            </p>

            <p>&nbsp;</p>
        </div>
    </div> <!-- end product-section -->

@endsection

@section('extra-js')
    <script>
        (function(){
            const currentImage = document.querySelector('#currentImage');
            const images = document.querySelectorAll('.product-section-thumbnail');

            images.forEach((element) => element.addEventListener('click', thumbnailClick));

            function thumbnailClick(e) {
                currentImage.classList.remove('active');

                currentImage.addEventListener('transitionend', () => {
                    currentImage.src = this.querySelector('img').src;
                    currentImage.classList.add('active');
                })

                images.forEach((element) => element.classList.remove('selected'));
                this.classList.add('selected');
            }

        })();
    </script>

    <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="{{ asset('js/algolia.js') }}"></script>

@endsection
