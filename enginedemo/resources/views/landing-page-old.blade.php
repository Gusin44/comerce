<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Demo Example</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    </head>
    <body>
        <div id="app">
            <header class="with-background">
                <div class="top-nav container">
                    <div class="top-nav-left">
                        <!-- <div class="logo">Demo</div> -->
                        {{ menu('main', 'partials.menus.main') }}
                    </div>
                    <div class="top-nav-right" style="display: block;">
                        @include('partials.menus.main-right')
                    </div>
                </div> <!-- end top-nav -->
                <div class="hero container">
                    <div class="hero-copy">
                        <h1>Rocks at Klingking</h1>
                        <!-- <p>Kelingking beach in Nusa Penida , Bali is one of the most sought after attractions for first timers and day trip visitors to the pristine island for the famous T-Rex shape hill.</p> -->
                        <div class="hero-buttons">
                            <a href="#" class="button button-white">See More</a>
                        </div>
                    </div> <!-- end hero-copy -->

                    <div class="hero-image">
                        <!-- <img src="img/macbook-pro-laravel.png" alt="hero image"> -->
                    </div> <!-- end hero-image -->
                </div> <!-- end hero -->
            </header>

            <div class="featured-section">

                <div class="container">
                    <h1 class="text-center">Welcome</h1>

                    <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore vitae nisi, consequuntur illum dolores cumque pariatur quis provident deleniti nesciunt officia est reprehenderit sunt aliquid possimus temporibus enim eum hic lorem.</p>

                    <div class="text-center button-container">
                        <a href="#" class="button">Bali</a>
                        <a href="#" class="button">Lombok</a>
                    </div>

                    {{-- <div class="tabs">
                        <div class="tab">
                            Featured
                        </div>
                        <div class="tab">
                            On Sale
                        </div>
                    </div> --}}

                    <div class="products text-center">
                        @foreach ($products as $product)
                            <div class="product">
                                <a href="{{ route('shop.show', $product->slug) }}"><img src="{{ productImage($product->image) }}" alt="product"></a>
                                <a href="{{ route('shop.show', $product->slug) }}"><div class="product-name">{{ $product->name }}</div></a>
                                <div class="product-price">{{ $product->presentPrice() }}</div>
                            </div>
                        @endforeach

                    </div> <!-- end products -->

                    <div class="text-center button-container">
                        <a href="{{ route('shop.index') }}" class="button">View more products</a>
                    </div>

                </div> <!-- end container -->

            </div> 
            <!-- end featured-section -->

            <div class="blog-section">
                <div class="container">
                    <h1 class="text-center">From Our Blog</h1>

                    <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore vitae nisi, consequuntur illum dolores cumque pariatur quis provident deleniti nesciunt officia est reprehenderit sunt aliquid possimus temporibus enim eum hic.</p>

                    <div class="blog-posts">
                        <div class="blog-post">
                            <a href="post.link">
                                <img src="http://demo.local/img/products/pantai.jpg" alt="product">
                            </a>
                            <a href="post.link"><h2 class="blog-title">123</h2></a>
                            <div class="blog-description">123</div>
                        </div>
                        <div class="blog-post">
                            <a href="post.link">
                                <img src="http://demo.local/img/products/pantai.jpg" alt="product">
                            </a>
                            <a href="post.link"><h2 class="blog-title">123</h2></a>
                            <div class="blog-description">123</div>
                        </div>
                        <div class="blog-post">
                            <a href="post.link">
                                <img src="http://demo.local/img/products/pantai.jpg" alt="product">
                            </a>
                            <a href="post.link"><h2 class="blog-title">123</h2></a>
                            <div class="blog-description">123</div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div> 
            <!-- end blog-section -->

            @include('partials.footer')

        </div> <!-- end #app -->
        <script src="js/app.js"></script>
    </body>
</html>
