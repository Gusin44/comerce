<footer style="
    background: transparent;
    right: 0;
    position: fixed;
    bottom: 0;
    width: 100%;
">
    <div class="footer-content container">
        <!-- <div class="made-with">Powered by <i class="fa fa-heart heart"></i> dionlinein.com</div> -->
        {{ menu('footer', 'partials.menus.footer') }}
    </div> <!-- end footer-content -->
</footer>
