@if(@$background)
<header style="background:url({{@$background}});background-size:cover;">
@else
<header>
@endif
    <div class="top-nav container">
      <div class="top-nav-left">
          <!-- <div class="logo"><a href="/">Demo</a></div> -->
          @if (! (request()->is('checkout') || request()->is('guestCheckout')))
          {{-- menu('main', 'partials.menus.main') --}}
          @endif
      </div>
      <div class="top-nav-right" style="display: block;">
          @if (! (request()->is('checkout') || request()->is('guestCheckout')))
          @include('partials.menus.main-right')
          @endif
      </div>
    </div> <!-- end top-nav -->
</header>
