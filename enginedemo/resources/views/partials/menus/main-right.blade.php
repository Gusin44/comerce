<ul>
    @guest
    <li style="font-size: 14px;margin: 0;"><a href="{{ route('users.edit') }}">My Account</a></li>
    @else
    <li style="font-size: 14px;">
        <a href="{{ route('users.edit') }}">My Trips</a>
    </li>
    <li style="font-size: 14px;margin: 0 0px 0px 10px;">
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            Logout
        </a>
    </li>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    @endguest
    
</ul>

<ul style="float: right;margin-top: 10px;">
    <li style="font-size: 10px;margin-right: 10px;">
        <a href="{{ route('cart.index') }}">My Wishlist</a>
    </li>
    <li style="font-size: 10px;"><a href="{{ route('cart.index') }}">My Trip Basket
    @if (Cart::instance('default')->count() > 0)
    ( {{ Cart::instance('default')->count() }} )
    @endif
    </a></li>
    {{-- @foreach($items as $menu_item)
        <li>
            <a href="{{ $menu_item->link() }}">
                {{ $menu_item->title }}
                @if ($menu_item->title === 'Cart')
                    @if (Cart::instance('default')->count() > 0)
                    <span class="cart-count"><span>{{ Cart::instance('default')->count() }}</span></span>
                    @endif
                @endif
            </a>
        </li>
    @endforeach --}}
</ul>
