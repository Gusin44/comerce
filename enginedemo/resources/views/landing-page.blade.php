<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Demo Example</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    </head>
    <body>
        <div id="app">
            <header class="with-background" style="height: 100%;width: 100%;position: fixed;background: url('{{urlImg($page->image)}}');background-size: cover;">
                <div class="top-nav container">
                    <div class="top-nav-left">
                        <!-- <div class="logo">Demo</div> -->
                        {{ menu('main', 'partials.menus.main') }}
                    </div>
                    <div class="top-nav-right" style="display: block;">
                        @include('partials.menus.main-right')
                    </div>
                </div> <!-- end top-nav -->
                <div style="position: fixed;bottom: 0;right: 10%;margin: 0 0 20% 0;">
                    <div class="hero-copy">
                        <!-- <h1>Rocks at Klingking</h1> -->
                        <!-- <p>Kelingking beach in Nusa Penida , Bali is one of the most sought after attractions for first timers and day trip visitors to the pristine island for the famous T-Rex shape hill.</p> -->
                        <div class="hero-buttons">
                            <a href="{{route('pages.show', $page->slug)}}" class="button button-white">{{$page->excerpt}}</a>
                        </div>
                    </div> <!-- end hero-copy -->

                    <div class="hero-image">
                        <!-- <img src="img/macbook-pro-laravel.png" alt="hero image"> -->
                    </div> <!-- end hero-image -->
                </div> <!-- end hero -->
                @include('partials.footer')
            </header>


        </div> <!-- end #app -->
        <script src="js/app.js"></script>
    </body>
</html>
