<?php

use App\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Category::insert([
            ['name' => 'Beach', 'slug' => 'sea', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'Forest', 'slug' => 'tree', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
