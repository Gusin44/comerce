<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Laptops
        for ($i=1; $i <= 10; $i++) {
            Product::create([
                'name' => 'Pantai '.$i,
                'slug' => 'sea-'.$i,
                'details' => 'White Sand',
                'price' => rand(15000000, 25000000),
                'description' =>'Lorem '. $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
                'image' => 'products/dummy/pantai.jpg',
                'images' => '["products\/dummy\/pantai.jpg","products\/dummy\/pantai.jpg","products\/dummy\/pantai.jpg"]',
            ])->categories()->attach(1);
        }

        // Select random entries to be featured
        Product::whereIn('id', [1, 2,3,4])->update(['featured' => true]);
    }
}
